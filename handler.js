var events = require('events')
var eventEmitter = new events.EventEmitter();


var myHandler1 = function()
{
    console.log("Hello");
    eventEmitter.emit('event2');

}

var Myhandler2 = function()
{
    console.log("this is event handler function");
}

eventEmitter.on('event1',myHandler1);

eventEmitter.on('event2', Myhandler2);

eventEmitter.emit('event1');

