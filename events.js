//EventEmitter is a class not a method.
const EventEmitter = require ('events');
//this is object->
const emitter = new EventEmitter();

//register a listner
emitter.on('messageLogged',function(arg)
{
     console.log('listner called',arg);
});

//raise an event--
//emitter.emit('messageLogged')
//emit stands for making a noise produce - nd provide singalling--

//events argument--
emitter.emit('messageLogged',{id: 1,url: 'http//'});    



