//React is by default component
import React from 'react';

const Menu = function()
{
    return(
        <div className="Menustyle">
            <ul>
                <li>Home</li>
                <li>About</li>
                <li>Contact</li>
            </ul>
        </div>
    )
}

//default is used for React bcz React is also by default

export default Menu; 